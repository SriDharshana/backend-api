from flask import Flask, Response, request,jsonify
import pymongo
import random
import json
from bson.json_util import dumps
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
from bson.objectid import ObjectId
try:
    mongo=pymongo.MongoClient(host="localhost",port=27017)
    db = mongo.studentdetails
    mongo.server_info()
except:
    print("ERROR -CANNOT CONNECT TO DB")



#####################CREATING    
@app.route("/details", methods=["POST"])
def create_user():

    try:
        # generate a student ID
        student_id = random.randint(10000, 99999)
        user = {
                    "student_id": student_id,
                    "name": request.get_json()["name"],
                    "class": request.get_json()["class"],
                    "email": request.get_json()["email"],
                    "DOB": request.get_json()["DOB"],
                    "phno": request.get_json()["phno"]
                }        
        dbResponse = db.users.insert_one(user)
        print(dbResponse.inserted_id)
        for attr in dir(dbResponse):
            print(attr)
        return Response(
            # include the generated student ID in the response 
            response=json.dumps({"message":"user created", "id":f"{dbResponse.inserted_id}","student_id": student_id  }),
            status=200,
            mimetype="application/json"
        )
    except Exception as ex:
        print("**********")
        print(ex)
        print("********")

########GET ALL DETAILS
@app.route("/details", methods=["GET"])
def get_some_users():
    try:
        data=list(db.users.find())
        print(data)
        for user in data:
            user["_id"]=str(user["_id"])
        return Response(response=json.dumps(data),
                        status=200,
                        mimetype="application/json"
        )

    except Exception as ex:
        print(ex)
        return Response(response=json.dumps({"message":"cannot read users"}),status=500,mimetype="application/json"
        )
    
##############################
#GET THE PARTICULAR USER DETAILS USING THEIR ID

@app.route("/details/<int:student_id>", methods=["GET"])
def get_particular_users_(student_id):
    try:
        info_students = db.users.find_one({"student_id": student_id})
        resp = dumps(info_students)
        return Response(resp,status=200, mimetype='application/json')
    except Exception as ex:
        print(ex)
        return jsonify({'error': 'Could not retrieve user details'}), 500

######################################
#updating details

@app.route('/update/<int:student_id>', methods=['PUT'])
def update(student_id):
    try:
        update_data = request.get_json()
        db.users.update_one({'student_id': student_id}, {'$set': update_data})
        return jsonify('User updated successfully!')
    except Exception as ex:
        print(ex)
        return jsonify({'error': 'Could not update user details'}), 500

######################################
#deleting student details
@app.route("/details/<int:id>", methods=["DELETE"])
def delete_info(id):
    try:
        db.users.delete_one({'student_id':id})
        return Response(
            response=json.dumps({"message":"deleted successfully"}),
            status = 200
        )
    
    except Exception as ex:
        print("\n----------------------------------\n",ex,"\n---------------------------------------\n")

######################
@app.route("/register", methods=["POST"])
def login_user():
    try:
        # generate a student ID
        user={"username":request.form["username"],"password":request.form["password"]}
        dbResponse = db.loginDetails.insert_one(user)
        print(dbResponse.inserted_id)
        for attr in dir(dbResponse):
            print(attr)
        return Response(
            # include the generated student ID in the response 
            response=json.dumps({"message":"user created", "id":f"{dbResponse.inserted_id}" }),
            status=200,
            mimetype="application/json"
        )
    except Exception as ex:
        print("**")
        print(ex)
        print("**")

######################
#authenticating login
@app.route("/login", methods=["POST"])
def check_login():
    try:
        requestJson = request.json
        username = requestJson['username']
        loginpwd = requestJson['password']

        login_creds = db.loginDetails.find_one({"username":username})

        if login_creds['password'] == loginpwd:
            message = 'Successful'
        else:
            message = 'Unsuccessful'
        resp_str = "Authentication was " + message
        return Response(
            response=json.dumps({"message": resp_str}),
            status = 200
        )   

    except Exception as ex:
        print("\n---------------------------------\n",ex,"\n-----------\n")

######################
if __name__ == "__main__":
    app.run(port=5000, debug=True)
